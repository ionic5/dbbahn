import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeparaturesPage } from './deparatures.page';

const routes: Routes = [
  {
    path: '',
    component: DeparaturesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeparaturesPageRoutingModule {}
