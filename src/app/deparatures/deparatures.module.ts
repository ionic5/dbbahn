import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeparaturesPageRoutingModule } from './deparatures-routing.module';

import { DeparaturesPage } from './deparatures.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeparaturesPageRoutingModule
  ],
  declarations: [DeparaturesPage]
})
export class DeparaturesPageModule {}
