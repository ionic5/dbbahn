import { Component, OnInit } from '@angular/core';
import { FahrplanService } from '../fahrplan.service';
import { ActivatedRoute } from '@angular/router';
import { Util } from 'src/Util';

@Component({
  selector: 'app-deparatures',
  templateUrl: './deparatures.page.html',
  styleUrls: ['./deparatures.page.scss'],
})
export class DeparaturesPage implements OnInit {
  station: string;
  board: any;

  constructor(private activatedRoute: ActivatedRoute, private fahrplanService: FahrplanService) { }

  ngOnInit() {
    this.station = this.activatedRoute.snapshot.paramMap.get('name');
    // tslint:disable-next-line: radix
    const id: number = parseInt(this.activatedRoute.snapshot.paramMap.get('id')); // Get id from route (url).
    // Create date in format yyyy-mm-dd to retrieve arrivals for this date.
    const now = new Date().toISOString().slice(0, 10);
    this.fahrplanService.getArrivals(id, now).subscribe(data => {
      // Loop through JSON results and replace special characters (e.g. &#x0028 to left bracket).
      // Format date also to display just time with leading zeros.
      data.forEach(element => {
        element.origin = Util.replaceSpecialchars(element.origin);
        element.dateTime = Util.convertISOTimeToHHMM(element.dateTime);
      });
      this.board = data;
    });
  }
}
