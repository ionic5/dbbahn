import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { DeparaturesPage } from './deparatures.page';
import { RouterModule } from '@angular/router';
import { IonicStorageModule } from '@ionic/storage';

describe('DeparaturesPage', () => {
  let component: DeparaturesPage;
  let fixture: ComponentFixture<DeparaturesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeparaturesPage ],
      imports: [
        IonicStorageModule.forRoot(),
        HttpClientModule,
        IonicModule.forRoot(),
        RouterModule.forRoot([])
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(DeparaturesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
