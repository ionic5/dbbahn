import { Component, OnInit } from '@angular/core';
import { FahrplanService } from '../fahrplan.service';
import { Router } from '@angular/router';

/**
 * TODO:
 * - Add error handling to https API calls.
 * - Implement settings.
 * - Localization?
 */

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  stations: any; // Array containing displayed stations.
  search: string; // Variable for holding data for seach criteria (location) for getting information about stations and timetables.

  constructor(private fahrplanService: FahrplanService, private router: Router) {}

  ngOnInit() {
    // Retrieve location used as search term. This is used as default value on search field and list of stations.
    this.fahrplanService.getLocation().then (data => {
      this.search = data;
      if (!this.search) { // If there is no search term (app is used for the first time), Berlin is default location.
        this.search = 'Berlin';
      }
      this.getStations(); // Update list of stations on UI.
    });
  }

  /**
   * Executes find (retrieves stations) when user hits enter.
   * 
   * @param e Event for keypress.
   */
  find(e) {
    if (e.keyCode === 13) { // If enter is presssed, retrieve stations.
      this.getStations(); // Update list of stations on UI.
    }
  }

  /**
   * Navigate to settings page.
   */
  show_settings() {
    this.router.navigateByUrl('settings');
  }

  /**
   * Navigate to map page.
   */
  show_map() {
    this.router.navigateByUrl('map');
  }

  /**
   * Navigate to arrivals page.
   * 
   * @param station Selected station, which is used to get arrivals (timetable).
   */
  show_arrivals(station: any) {
    this.router.navigateByUrl('arrivals/' + station.id + '/' + station.name);
  }

  /**
   * Navigate to deparatures page.
   * 
   * @param station Selected station, which is used to get deparatures (timetable).
   */
  show_deparatures(station: any) {
    this.router.navigateByUrl('deparatures/' + station.id + '/' + station.name);
  }

  /**
   * Retieves stations based on seach criteria (location). Updates array used on UI to display stations.
   */
  private getStations() {
    this.fahrplanService.setLocation(this.search); // Set search term as location, which is used to get nearby stations.
    this.fahrplanService.getStations().subscribe(data => { // Retrieve stations.
      this.stations = data;
    });
  }


}
