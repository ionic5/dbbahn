import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { HomePage } from './home.page';
import { MapPage } from '../map/map.page';
import { ArrivalsPage } from '../arrivals/arrivals.page';
import { DeparaturesPage } from '../deparatures/deparatures.page';
import { IonicStorageModule } from '@ionic/storage';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePage, MapPage, ArrivalsPage, DeparaturesPage ],
      imports: [
        IonicStorageModule.forRoot(),
        IonicModule.forRoot(),
        HttpClientModule,
        RouterTestingModule.withRoutes (
          [
            {path: 'map', component: Map},
            {path: 'arrivals', component: ArrivalsPage},
            {path: 'deparatures', component: DeparaturesPage}
          ]
        ),
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
