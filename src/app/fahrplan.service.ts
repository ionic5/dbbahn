import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})

/**
 * Class contains functionality to retrieve required information about Deutche Bahn's stations and timetables.
 * Free API is used to retrieve data. Data is retrieved based on location (seach criteria). Used location is saved into
 * local storage.
 */
export class FahrplanService {
  // Member variable holding information about the location used to search for stations and timetables (e.g. Berlin).
  location: string;

  constructor(private http: HttpClient, private storage: Storage) { }

  /**
   *  Return location used to search for stations and timetables. Method retrieves value from 
   *  local storage.
   */
  public async getLocation(): Promise<string> {
    if (!this.location) {
      // Since retrieving value from local storage is always asynchronous, await is used to return a promise.
      this.location = await this.storage.get('location');
      return this.location;
    } else {
      return this.location;
    }
  }

  /**
   * Sets new value for location used to get information about stations and timetables. Value will be stored
   * into local storage.
   * @param newLocation New location (search criteria) as string.
   */
  public setLocation(newLocation: string) {
    this.location = newLocation;
    this.storage.set('location', newLocation);
  }

  /**
   *  Retrieve all stations based on location.
   */
  public getStations(): Observable<any> {
    const address = environment.locationUrl + this.location;
    return this.http.get(address);
  }

  /**
   * Retrieve arrivals (timetable).
   * 
   * @param id Id (location) used to retrieve timetable.
   * @param date Date used as criteria to retrieve timetable.
   */
  public getArrivals(id: number, date: string): Observable<any> {
    const address = environment.arrivalUrl + id + '?date=' + date;
    return this.http.get(address);
  }

  /**
   * Retrieve deparatures (timetable).
   * 
   * @param id Id (location) used to retrieve timetable.
   * @param date Date used as criteria to retrieve timetable.
   */
  public getDeparatures(id: number, date: string): Observable<any> {
    const address = environment.deparatureUrl + id + '?date=' + date;
    return this.http.get(address);
  }
}
