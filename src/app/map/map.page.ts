import { Component, OnInit } from '@angular/core';
import { Map, tileLayer } from 'leaflet';
import L from 'leaflet';
import 'leaflet/dist/images/marker-shadow.png';
import 'leaflet/dist/images/marker-icon-2x.png';
import { LocationService } from '../location.service';
import { FahrplanService } from '../fahrplan.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
  title = 'DB - Map';
  map: Map;
  lat: number;
  lng: number;

  constructor(private locationService: LocationService, private fahrplanService: FahrplanService) { }

  ngOnInit() {
    this.lat = 65;
    this.lng = 25;

    // Get location (on phone using GPS) to display user's location on map.
    this.locationService.getLocation().then(data => {
      this.lat = data.coords.latitude;
      this.lng = data.coords.longitude;
      // Translate coordinates to location name using an API.
      this.locationService.getLocationName(this.lat, this.lng).subscribe(value => {
        this.title = value.results[0].formatted;
      });
    });

    this.loadMap(); // Load map when pages is displayed.
  }

  /**
   * Load OpenStreetMap with user's location (if enabled) and markers displaying stations based on location
   * (which is given on home page).
   */
  private loadMap() {
    setTimeout(()=> {
      this.map = new Map('map').setView([this.lat,this.lng], 8);
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attributions: '',
        maxZoom: 18
      }).addTo(this.map);

      this.addStationsAsMarkers(); // Add stations as markers to the map.

      this.map.locate({
        setView: true,
        maxZoom: 10
      }).on('locationfound', (e) => { // This event will fire if GPS is enabled and location is found.
        const markerGroup = L.featureGroup();
        this.addMarker(markerGroup, e.latitude, e.longitude);
        this.map.addLayer(markerGroup);
      });
    }, 50);
  }

  /**
   * Add station locations as markers to the map. Location (search )is set on home page.
   */
  private addStationsAsMarkers() {
    const markerGroup = L.featureGroup();
    this.fahrplanService.getStations().subscribe(data => { // Retrieve stations.
      data.forEach(element => { // Loop through found stations and add markers.
        this.addMarker(markerGroup,element.lat, element.lon);
      });
    });
    this.map.addLayer(markerGroup);
  }

  /**
   * Add marker to map.
   * 
   * @param markerGroup Marker group, where the marker is added.
   * @param lat Latitude for the marker.
   * @param lon  Longitude for the marker.
   */
  private addMarker(markerGroup: any, lat: number, lon: number) {
    const marker: any = L.marker([lat, lon]);
    markerGroup.addLayer(marker);
  }
}
