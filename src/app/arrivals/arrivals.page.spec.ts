import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { ArrivalsPage } from './arrivals.page';
import { RouterModule } from '@angular/router';
import { IonicStorageModule } from '@ionic/storage';

describe('ArrivalsPage', () => {
  let component: ArrivalsPage;
  let fixture: ComponentFixture<ArrivalsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArrivalsPage ],
      imports: [
        IonicStorageModule.forRoot(),
        HttpClientModule,
        IonicModule.forRoot(),
        RouterModule.forRoot([])
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ArrivalsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
