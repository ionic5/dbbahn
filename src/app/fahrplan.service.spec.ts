import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { FahrplanService } from './fahrplan.service';
import { IonicStorageModule } from '@ionic/storage';


describe('FahrplanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        IonicStorageModule.forRoot(),
        HttpClientModule
      ]
    });
  });

  it('should be created', () => {
    const service: FahrplanService = TestBed.get(FahrplanService);
    expect(service).toBeTruthy();
  });

  it ('should get data', (done) => {
    const service: FahrplanService = TestBed.get(FahrplanService);
    service.setLocation('Berlin');

    service
      .getStations()
      .subscribe(data => {
        const result = data.status;
        const items: any = data;
        expect(Array.isArray(items)).toBeTruthy();
        expect(items.length).toBeGreaterThan(0);
        done(); // Use done in asynchronous test to get rid off error saying test has no expectations.
      });
  });
});
