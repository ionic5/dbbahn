// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  locationUrl: 'https://api.deutschebahn.com/freeplan/v1/location/',
  arrivalUrl: 'https://api.deutschebahn.com/freeplan/v1/arrivalBoard/',
  deparatureUrl: 'https://api.deutschebahn.com/freeplan/v1/deparatureBoard/',
  geocodingUrl: 'https://api.opencagedata.com/geocode/v1/json?',
  geocodingApiKey: '7dbd623992de4313b491d8b464ff0529' // DO NOT USE MY APIKEY, IF USING SERVICE FROM OPENGAGE!!!
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
