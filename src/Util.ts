/**
 * Utitily class containing usefull static methods.
 */

export class Util {
  /**
   * Replaces special characters (e.g. &#x0029; which is left bracket) with displayable counterparts.
   * 
   * @param str String possibly containing special characters.
   */
  static replaceSpecialchars (str: string): string {
    const result = 
    str
      .replace('&#x0029;', ')')
      .replace('&#x0028;', '(');
    return result;
  }

  /**
   * Converts date and time in ISO format to time in hh.mm format.
   * 
   * @param isoTime Date and time in ISO format.
   */
  static convertISOTimeToHHMM(isoTime: string): string {
    const date = new Date(isoTime);

    const hLeadingZero = date.getHours() < 10 ? '0' : '';
    const hrs = date.getHours().toString();
    const hours = hLeadingZero + hrs;

    const mLeadingZero = date.getMinutes() < 10 ? '0' : '';
    const mins = date.getMinutes().toString();
    const minutes = mLeadingZero + mins;

    return hours + '.' + minutes;
  }
}
